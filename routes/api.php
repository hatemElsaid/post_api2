<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login','Api\ApiController@login');
Route::post('register','Api\ApiController@register');

// Route::group(['middleware' => 'auth:api'], function(){
// 	Route::post('details', 'Api\ApiController@details');
// });
// Route::get('home' , "Api\ApiController@home");
// Route::get('index' , "Api\Post\PostController@index");
// Route::get('view/{id}' , "Api\Post\PostController@view")->name('post_view');
// Route::get('edit/{id}' , "Api\Post\PostController@edit")->name('post_edit');
 
Route::apiResource('posts','Api\Post\PostController');
Route::get('categories','Api\Post\PostController@categories');
Route::get('/like/{id}' , "Api\Post\PostController@like")->name('like');
Route::get('/dislike/{id}' , "Api\Post\PostController@disLike")->name('dislike');