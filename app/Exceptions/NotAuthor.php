<?php

namespace App\Exceptions;

use Exception;

class NotAuthor extends Exception
{
    public function render(){
        return[
            "data"=>"this not belongs to User";
        ];
    }
}
