<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class HomeResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            
           'post_title' => $this->post_title,
           'post_body' => $this->post_body,
           'author' => $this->user_name,
           'likes' => $this->likes,
           'dislikes' => $this->dislikes,
           'post image' => $this->post_image,
           'category_id' => $this->category_id,
           'post views' => $this->views,
           // 'view_post' => route('post_view',$this->id),
           // 'post_edit' => route('post_edit',$this->id)
           'links'=>[
            "view post" =>route('posts.show',$this->id),
            ]
           
           
          ];
    }
}
