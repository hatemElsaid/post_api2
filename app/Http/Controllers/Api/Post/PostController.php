<?php

namespace App\Http\Controllers\Api\Post;
use App\Http\Resources\PostResource;
use App\Post;
use App\Http\Resources\PostCollection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\HomeResource;
use App\Http\Resources\CategoryResource;
use Symfony\Component\HttpFoundation\Response;


use Illuminate\Support\Facades\Input;//need to upload Image
use Illuminate\Support\Facades\URL;//need to upload Image
use Illuminate\Support\Facades\File;//need to upload Image
use Illuminate\Support\Facades\DB;
use App\Exceptions\NotAuthor;
use App\Http\Requests\PostRequest;
use App\Exceptions\ProductNotBelongsToUser;

 
 
use App\Category;
 
use App\Like;
use App\Dislike;
use Auth;

class PostController extends Controller
{

     public function __construct(){
         $this->middleware('auth:api')->except('index','show','categories');
     }
    // public function index(){
    //     $posts=Post::all();
         
         
    //     return PostResource($posts);
         
    // }
    // public function view($post_id, Request $request){
    //     $posts =Post::where('id' , '=' ,$post_id)->get();
    //     $likePost =Post::find($post_id);
    //     $likeCount =Like::where(['post_id'=>$likePost->id])->count();
    //     $disLikeCount = Dislike::where(['post_id'=>$likePost->id])->count();
         
    //     $categories = Category::all();
    //     $post= Post::find($post_id);
    //   //   $post->addVisit(); //very important : add new view ti DB  usin(cyrildewit/laravel-page-visits-counter)
        
    //  $total_view=$post->views;
    //  $likedisLikeCount=$likeCount-$disLikeCount;
    //  $recommend = $total_view+$likeCount-$disLikeCount;
       
    //   $post->recommend= $recommend;
    //   $post->likes = $likeCount;
    //   $post->dislikes = $disLikeCount;
    //   $post->like_dislike = $likedisLikeCount;
    //   $post->views+=1;
    //   $post->update(); 
    //     return PostResource::collection($posts);  
         
    // }
    // public function edit($post_id){
    //       if($post_id){
    //     $categories=Category::all();
    //     $posts = Post::where('id' , '=' ,$post_id)->get();;
        
    //     return PostResource::collection($posts);
    //       }else{
    //           return "";
    //       }
    // }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post =Post::all();
        return HomeResource::collection($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    { 
        $post = new Post;
        $post->post_title = $request->title;
        $post->user_id = Auth::user()->id;
        $post->user_name = Auth::user()->name;
        $post->post_body = $request->body;
        $post->category_id = $request->category_id;

                if (Input::hasFile('post_image')){
                    $file = Input::file('post_image');
                $file->move(public_path().'/posts/', //posts folder we and create it in public folder
                $file->getClientOriginalName());
                $url = URL::to('/').'/posts/'.$file->getClientOriginalName();
                $post->post_image =$url;
                }

        
        $this->validate($request , ['category_id'=>'exists:categories,id']);
        $post->save();
        return response(['data'=>new PostResource($post)],Response::HTTP_CREATED);
    }

  

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if($post){
        return new PostResource($post);
        }else{
            "Post NotFound";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $this->checkUser($post);
         
        $request['title']=$request->post_title;
        $request['body']=$request->post_body;

         $post->update($request->all());
         
         return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->checkUser($post);
        $post->delete();
        return "Post Deleted";
    }

     

public function like($id){
     $loggedIn_user = Auth::user()->id;
  
    $like_user = Like::where(["user_id"=>$loggedIn_user , "post_id" =>$id])->first();
     
    if(empty($like_user->user_id)){
        $user_id = Auth::user()->id;
        $email = Auth::user()->email;
        $post_id = $id;
        $like = new Like;
        $like->user_id = $user_id;
        $like->email= $email;
        $like->post_id = $post_id;
        $like->save();
        return   redirect()->route('profile', ['id' => $id]);
        
    }else{
        $like_user->delete();
        return redirect()->back();
    }
}
    public function disLike($id){  //like and dislike function are the same functionality
        $loggedIn_user = Auth::user()->id;
        $like_user = Dislike::where(["user_id"=>$loggedIn_user , "post_id" =>$id])->first();
        if(empty($like_user->user_id)){
            $user_id = Auth::user()->id;
            $email = Auth::user()->email;
            $post_id = $id;
            $like = new Dislike;
            $like->user_id = $user_id;
            $like->email= $email;
            $like->post_id = $post_id;
            $like->save();
             
            
         return   redirect()->route('profile', ['id' => $id]);
            
        }else{
            $like_user->delete();
            return redirect()->back();
        }
    }
    public function categories(){
        $categories = Category::all();
        return CategoryResource::Collection($categories);
    }
      public function checkUser($post){
        if(Auth::id()!==$post->user_id)
        {
            throw new ProductNotBelongsToUser;
            

        }
    }
}
